package main

import "fmt"

type Node struct {
	Val int
	Next *Node
}

// 使用队列实现
type RecentCounter struct {
	Size int
	Head *Node
	Tail *Node
}


func Constructor() RecentCounter {
	return RecentCounter{
		Size: 0,
		Head: nil,
		Tail: nil,
	}
}


func (this *RecentCounter) Ping(t int) int {
	node := &Node{
		Val:  t,
		Next: nil,
	}

	// 队尾添加数据
	if this.Tail == nil {
		this.Tail = node
		this.Head = node
	} else {
		this.Tail.Next = node
		this.Tail = this.Tail.Next
	}

	this.Size++

	for this.Head.Val < t - 3000  {
		// 循环处理队列中每一个的值, 如果不符合要求的值则删除
		head := this.Head
		this.Head = head.Next
		head = nil
		this.Size--
	}

	return this.Size
}


func main() {
	recentCounter:=Constructor()
	ret := recentCounter.Ping(1)     // requests = [1]，范围是 [-2999,1]，返回 1
	fmt.Println(ret)
	ret = recentCounter.Ping(100)   // requests = [1, 100]，范围是 [-2900,100]，返回 2
	fmt.Println(ret)
	ret = recentCounter.Ping(3001)  // requests = [1, 100, 3001]，范围是 [1,3001]，返回 3
	fmt.Println(ret)
	ret = recentCounter.Ping(3002)  // requests = [1, 100, 3001, 3002]，范围是 [2,3002]，返回 3
	fmt.Println(ret)
}
