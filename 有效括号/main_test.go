package main

import "testing"

// 示例 1:
//
// 输入: "()"
//输出: true
//
//
// 示例 2:
//
// 输入: "()[]{}"
//输出: true
//
//
// 示例 3:
//
// 输入: "(]"
//输出: false
//
//
// 示例 4:
//
// 输入: "([)]"
//输出: false
//
//
// 示例 5:
//
// 输入: "{[]}"
//输出: true

type TestObj struct {
	Str string
	Except bool
}

func TestIsValid(t *testing.T)  {
	testArr := [...]TestObj{
		{
			Str:    "()",
			Except: true,
		},
		{
			Str: "()[]{}",
			Except: true,
		},
		{
			Str: "(]",
			Except: false,
		},
		{
			Str: "([)]",
			Except: false,
		},
		{Str: "{[]}", Except: true},
		{Str: "", Except: true},
		{Str: "]", Except: false},
		{Str: "[", Except: false},
		{Str: "({[)", Except: false},
	}

	for _, obj := range testArr {
		if ret := IsValid(obj.Str); ret != obj.Except {
			t.Errorf("string is %s, except %t, but got %t", obj.Str, obj.Except, ret)
		}
	}
}


