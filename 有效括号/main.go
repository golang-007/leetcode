package main

import (
	"fmt"
)

//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
//
// 有效字符串需满足：
//
//
// 左括号必须用相同类型的右括号闭合。
// 左括号必须以正确的顺序闭合。
//
//
// 注意空字符串可被认为是有效字符串。
//
// 示例 1:
//
// 输入: "()"
//输出: true
//
//
// 示例 2:
//
// 输入: "()[]{}"
//输出: true
//
//
// 示例 3:
//
// 输入: "(]"
//输出: false
//
//
// 示例 4:
//
// 输入: "([)]"
//输出: false
//
//
// 示例 5:
//
// 输入: "{[]}"
//输出: true
// Related Topics 栈 字符串
// 👍 2073 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
func IsValid(s string) bool {
	stack := []string{}
	size := 0

	for _,v := range s {
		if size == 0 {
			stack = append(stack, string(v))
			size++
			continue
		}

		if string(v) == ")" && stack[size - 1] == "(" {
			stack = stack[:size-1]
			size--
			continue
		}
		if string(v) == "}" && stack[size - 1] == "{" {
			stack = stack[:size-1]
			size--
			continue
		}
		if string(v) == "]" && stack[size - 1] == "[" {
			stack = stack[:size-1]
			size--
			continue
		}
		stack = append(stack, string(v))
		size++
	}
	//fmt.Println(stack)
	return size == 0
}

func main() {
	str := "()[]{}"
	ret := IsValid(str)
	fmt.Println(ret)
}
