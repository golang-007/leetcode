package main


type ListNode struct {
	Val int
	Next *ListNode
}

func ReverseList(head *ListNode) *ListNode {
	if head == nil {
		return head
	}

	var  newListNode *ListNode = nil

	for head != nil {
		next := head.Next
		head.Next = newListNode
		newListNode = head
		head = next
	}

	return newListNode
}

func main() {
	
}
