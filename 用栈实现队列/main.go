package main

import "fmt"

type MyQueue struct {
	Stack []int
	Size int // 队列的长度
}


/** Initialize your data structure here. */
func Constructor() MyQueue {
	return MyQueue{
		Stack: nil,
		Size:  0,
	}
}


/** Push element x to the back of queue. */
func (this *MyQueue) Push(x int)  {
	this.Stack = append(this.Stack, x)
	this.Size++
}


/** Removes the element from in front of queue and returns that element. */
func (this *MyQueue) Pop() int {
	if this.Size == 0 {
		return 0
	}
	val := this.Stack[0]
	this.Stack = this.Stack[1:len(this.Stack)]
	this.Size--
	return val
}


/** Get the front element. */
func (this *MyQueue) Peek() int {
	if this.Size == 0 {
		return 0
	}
	return this.Stack[0]
}


/** Returns whether the queue is empty. */
func (this *MyQueue) Empty() bool {
	return this.Size == 0
}

func main() {
	//输入：
	//["MyQueue", "push", "push", "peek", "pop", "empty"]
	//[[], [1], [2], [], [], []]
	//输出：
	//[null, null, null, 1, 1, false]
	//
	//解释：
	myQueue := Constructor()
	myQueue.Push(1) // queue is: [1]
	myQueue.Push(2) // queue is: [1, 2] (leftmost is front of the queue)
	ret := myQueue.Peek() // return 1
	fmt.Println(ret)
	ret = myQueue.Pop() // return 1, queue is [2]
	fmt.Println(ret)
	isBool := myQueue.Empty() // return false
	fmt.Println(isBool)
}
