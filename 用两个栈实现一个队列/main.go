package main

import "fmt"

type Node struct {
	Val int
	Next *Node
}

// 连表实现
type CQueue struct {
	Head *Node
	Tail *Node
	Size int
}


func Constructor() CQueue {
	return CQueue{
		Head:    nil,
		Tail:    nil,
		Size:    0,
	}
}

// 尾部添加一个元素
func (this *CQueue) AppendTail(value int)  {
	node := &Node{
		Val:  value,
		Next: nil,
	}
	if this.Tail == nil {
		this.Tail = node
		this.Head = node
	} else {
		this.Tail.Next = node
		this.Tail = this.Tail.Next
	}

	this.Size++
}

// 头部删除一个元素
func (this *CQueue) DeleteHead() int {
	if this.Size == 0 {
		return -1
	}
	val := this.Head.Val
	this.Head = this.Head.Next
	this.Size--
	if this.Size == 0 {
		this.Head = nil
		this.Tail = nil
	}
	return val
}

func main() {

	// ["CQueue","deleteHead","appendTail","deleteHead","appendTail","appendTail","deleteHead","deleteHead","deleteHead","appendTail","deleteHead","appendTail","appendTail","appendTail","appendTail","appendTail","appendTail","deleteHead","deleteHead","deleteHead","deleteHead"]
	//[[],[],[12],[],[10],[9],[],[],[],[20],[],[1],[8],[20],[1],[11],[2],[],[],[],[]]

	cQueue := Constructor()
	cQueue.DeleteHead() //deleteHead
	cQueue.AppendTail(12) //appendTail 12
	cQueue.DeleteHead() //deleteHead
	fmt.Printf("%#v \n", cQueue)
	cQueue.AppendTail(10) //appendTail 10
	cQueue.AppendTail(9) //appendTail 9
	fmt.Printf("%#v \n", cQueue)
	cQueue.DeleteHead() //deleteHead
	cQueue.DeleteHead() //deleteHead
	cQueue.DeleteHead() //deleteHead
	cQueue.AppendTail(20) //appendTail 20
	cQueue.DeleteHead() //deleteHead
	cQueue.AppendTail(1) //appendTail 1
	cQueue.AppendTail(11) //appendTail 11
	cQueue.AppendTail(2) //appendTail 2
	cQueue.DeleteHead() //deleteHead
	cQueue.DeleteHead() //deleteHead
	cQueue.DeleteHead() //deleteHead
	cQueue.DeleteHead() //deleteHead
}
