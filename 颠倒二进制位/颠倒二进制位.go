package main

import (
	"fmt"
	"math"
)

func main() {
	//fmt.Printf("%b \n", uint32(0)) //将 0 转换为无符号的二进制
	//fmt.Printf("%b \n", uint32(31))// 将 31 转换为无符号的二进制
	//fmt.Println(10101010 & 00000001)
	ret := ReverseBits(uint32(10101010))
	fmt.Printf("%b\n%b",ret, math.MaxUint32)
}

func ReverseBits(num uint32) uint32 {
	ret := uint32(0) 	// 构建返回值
	power := uint32(31) // 构建循环次数
	for num != 0 {
		// 按位与(num & 1)取出每一位的值, 并移动到 power 对应的高位位置
		ret += (num & 1) << power
		// 右移一位, 相当于吧当前的数减少一位
		num = num >> 1

		// 控制位数的变化
		power--
	}

	return ret
}
