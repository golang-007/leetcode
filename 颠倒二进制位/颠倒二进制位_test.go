package main

import "testing"

type testRet struct {
	OriginV uint32
	ExpectV uint32
}

func TestReverseBits(t *testing.T)  {
	testMap := [...]testRet{
		{
			OriginV: 0b000010100101000001111010011100,
			ExpectV: 0b111001011110000010100101000000,
		},
		{
			0b11111111111111111111111111111101,
			0b10111111111111111111111111111111,
		},
		{
			0b1,
			0b1 << 31,
		},
	}

	for _, v := range testMap {
		b := ReverseBits(v.OriginV)
		if v.ExpectV != b {
			t.Errorf("input %b, get %b, expect %b", v.OriginV, b, v.ExpectV)
		}
	}
}
