package main

type ListNode struct {
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	//	使用快慢指针实现
	fast := head.Next
	slow := head

	if head == nil {
		return false
	}
	
	for fast != nil && fast.Next != nil {
		if fast == slow {
			return true
		}
		fast = fast.Next.Next
		slow = slow.Next
	}

	return false
}

func main() {

}
