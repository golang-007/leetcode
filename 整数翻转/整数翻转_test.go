package main

import "testing"

type testCase struct {
	Origin int
	Expect int
}

func TestReverse(t *testing.T) {
	testCaseArr := [...]testCase{
		{
			Origin: 0,
			Expect: 0,
		},
		{
			Origin: -123,
			Expect: -321,
		},
		{
			Origin: 92345798,
			Expect: 89754329,
		},
		{
			Origin:1534236469,
			Expect: 0,
		},
	}

	for _, val := range testCaseArr {
		expect := Reverse(val.Origin)
		if val.Expect != expect {
			t.Errorf("input %d, expected %d, but get %d", val.Origin, val.Expect, expect)
		}
	}
}