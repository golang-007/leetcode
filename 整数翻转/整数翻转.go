package main

import (
	"fmt"
	"math"
)

func main() {
	var x = -123
	result := Reverse(x)
	fmt.Println(result)
}

// 解法 1, 将数字转换为字符串,放入数组中, 数组方向遍历,重新组装为数字
func Reverse(x int) int {
	// 第一步: 是否超出范围, 不能超出边界值
	if x < math.MinInt32 || x > math.MaxInt32 {
		return 0
	}
	// 第二部: 判断数字的正负
	sign := 1
	if x < 0 {
		sign  = -1
		x = x * -1 // 如果 x 为负数, 则吧 x 转换为正数处理
	}

	result := 0
	// 做除法
	for x / 10 != x {
		result = result * 10 + x % 10
		x = x / 10
	}

	if result > math.MaxInt32 ||  result < math.MinInt32 {
		return 0
	}
	return result * sign
}

