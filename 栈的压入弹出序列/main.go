package main

import (
	"fmt"
)

//输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否为该栈的弹出顺序。假设压入栈的所有数字均不相等。例如，序列 {1,2,3,4,5} 是某栈
//的压栈序列，序列 {4,5,3,2,1} 是该压栈序列对应的一个弹出序列，但 {4,3,5,1,2} 就不可能是该压栈序列的弹出序列。
//
//
//
// 示例 1：
//
// 输入：pushed = [1,2,3,4,5], popped = [4,5,3,2,1]
//输出：true
//解释：我们可以按以下顺序执行：
//push(1), push(2), push(3), push(4), pop() -> 4,
//push(5), pop() -> 5, pop() -> 3, pop() -> 2, pop() -> 1
//
//
// 示例 2：
//
// 输入：pushed = [1,2,3,4,5], popped = [4,3,5,1,2]
//输出：false
//解释：1 不能在 2 之前弹出。
//
//
//
//
// 提示：
//
//
// 0 <= pushed.length == popped.length <= 1000
// 0 <= pushed[i], popped[i] < 1000
// pushed 是 popped 的排列。
//
//
// 注意：本题与主站 946 题相同：https://leetcode-cn.com/problems/validate-stack-sequences/
// 👍 106 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
func validateStackSequences(pushed []int, popped []int) bool {

	//listLen := len(pushed)
	var stack []int // 模拟近栈的数组
	for _, v := range pushed {
		stack = append(stack, v)
		for len(stack) > 0 && popped[0] == stack[len(stack) - 1] {
			// 说明 pop 正在出栈
			popped = popped[1:]
			stack = stack[:len(stack) - 1]
		}
	}

	return len(stack) == 0
}
//leetcode submit region end(Prohibit modification and deletion)

func main() {
	arr1 := []int{1,2,3,4,5}
	//arr2 := [...]int{5,4,3,2,1}
	//arr4 := []int{4,3,2,1,5}
	arr4 := []int{4,3,5,1,2}
	//4,3,5,1,2
	result := validateStackSequences(arr1, arr4)
	fmt.Println(result)

}
