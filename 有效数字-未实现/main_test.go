package main

import "testing"

// "0" => true
//" 0.1 " => true
//"abc" => false
//"1 a" => false
//"2e10" => true
//" -90e3 " => true
//" 1e" => false
//"e3" => false
//" 6e-1" => true
//" 99e2.5 " => false
//"53.5e93" => true
//" --6 " => false
//"-+3" => false
//"95a54e53" => false

type StrToBool struct {
	Key string
	Val bool
}

func TestIsNumber(t *testing.T)  {
	strToBool := [...]StrToBool{
		{
			"0",
			true,
		},
		{
			" 0.1 ",
			true,
		},
		{
			"abc",
			false,
		},
		{
			"1 a" ,
			false,
		},
		{
			"2e10",
			true,
		},
		{
			" -90e3 ",
			true,
		},
		{
			" -90e3 ",
			true,
		},
		{
			" 1e",
			false,
		},
		{
			"e3",
			false,
		},
		{
			" 6e-1",
			true,
		},
		{
			" 99e2.5 ",
			false,
		},
		{
			"53.5e93",
			true,
		},
		{
			" --6 ",
			false,
		},
		{
			"-+3",
			false,
		},
		{
			"95a54e53",
			false,
		},
		{
			".",
			false,
		},
		{
			".3",
			true,
		},
		{
			" ",
			false,
		},
		{
			"3.",
			true,
		},
		{
			"1 4",
			false,
		},
	}

	for _, v := range strToBool {
		vv := IsNumber(v.Key)
		if vv != v.Val {
			t.Errorf("input %s, get %t, but expect %t", v.Key, vv, v.Val)
		}
	}
}
