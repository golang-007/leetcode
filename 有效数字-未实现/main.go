package main

import (
	"fmt"
	"strings"
)

// "0" => true
//" 0.1 " => true
//"abc" => false
//"1 a" => false
//"2e10" => true
//" -90e3 " => true
//" 1e" => false
//"e3" => false
//" 6e-1" => true
//" 99e2.5 " => false
//"53.5e93" => true
//" --6 " => false
//"-+3" => false
//"95a54e53" => false


// 数字 0-9
// 指数 - "e"
// 正/负号 - "+"/"-"
// 小数点 - "."

// Related Topics 数学 字符串

func main() {
	// 分为几类
	// 有字母出现的 false, e 除外
	// 如果出现了e, 则后面必须为数字且为整数
	// 符号 +/- 可以出现在任何位置
	//s :=  strings.Trim( " -90e3 ", " ")
	//fmt.Printf("%t", s[0] == '-')

	ret := IsNumber("..") // false

	fmt.Println(ret)
}

func IsNumber(s string) bool {
	s = strings.TrimSpace(s) // 去除字符串中的空格

	ret := true
	if len(s) == 0 {
		return false
	}
	for i, v := range []byte(s) {
		switch {
		case (v == '-' || v == '+') && i+1 < len(s) && (s[i+1] == '-' || s[i+1] == '+'):
			// 如果出现了连续的-/+ 则返回 false
			ret = false
			break
		case v == 'e':
			// 如果为 e的情况, e 只能为数字或者 -1
			if i == 0 || (i == len(s) - 1) {
				ret = false
				break
			}
			//
			if i + 1 <= len(s) {
				ss := s[i+1:] //取出 e 后面的所有数字
				if ss[0] == '-' {
					ss = ss[1:]
				}
				for _, sv := range []byte(ss) {
					sv = sv-'0'
					if sv > '9' {
						ret = false
						break
					}
				}
			}
		case v == '.':
			// 处理点的问题, .后面的字符必须为数字
			if i == 0 {
				if len(s) == 1 || s[i+1] > '9' || s[i+1] < '0' {
					ret = false
					break
				}
			}

			// 不是最后一位的情况, 则前一位必须是数字
			if i > 0 && (s[i-1] > '9' || s[i-1] < '0') {
				ret = false
				break
			}

		case  v <= '9':
			continue
		case v > '9':
			ret = false
			break
		case v == ' ':
			// 处理空格的情况
			ret = false
			break
		}
		fmt.Println(i, ret)
	}

	return ret
}
