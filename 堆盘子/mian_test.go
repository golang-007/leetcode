package main

//["StackOfPlates","pop", "pop", "popAt", "popAt", "pop", "push", "push", "push", "push", "pop", "push", "push", "popAt", "pop", "popAt", "push", "popAt", "pop", "push", "pop", "pop", "pop", "popAt", "push", "pop", "popAt", "pop", "push", "popAt", "popAt", "push", "popAt", "popAt", "push", "pop", "popAt", "popAt", "popAt", "pop", "popAt", "popAt", "push", "popAt", "push", "push", "pop", "popAt", "popAt", "push", "popAt", "push", "pop", "pop", "push", "push", "push", "popAt", "popAt", "pop", "popAt", "pop", "pop", "push", "push"]
// [[6], [], [], [1], [3], [], [40], [10], [44], [44], [], [24], [42], [4], [], [0], [42], [5], [], [29], [], [], [], [0], [56], [], [4], [], [34], [1], [4], [52], [4], [6], [63], [], [6], [6], [1], [], [6], [2], [47], [1], [45], [52], [], [6], [6], [20], [4], [17], [], [], [43], [6], [30], [2], [3], [], [3], [], [], [51], [46]]


//测试结果:[null,-1,-1,-1,-1,-1,null,null,null,null,44,null,null,-1,42,10,null,-1,42,null,29,-1,-1,-1,null,56,-1,-1,null,-1,-1,null,-1,-1,null,63,-1,-1,-1,34,-1,-1,null,-1,null,null,52,-1,-1,null,-1,null,17,47,null,null,null,-1,-1,30,-1,43,-1,null,null]

//期望结果:[null,-1,-1,-1,-1,-1,null,null,null,null,44,null,null,-1,42,24,null,-1,42,null,29,44,10,40,null,56,-1,-1,null,-1,-1,null,-1,-1,null,63,-1,-1,-1,52,-1,-1,null,-1,null,null,52,-1,-1,null,-1,null,17,20,null,null,null,-1,-1,30,-1,6,43,null,null]

var stackOfPlates StackOfPlates
func main() {
	//[-1,42,10]
	stackOfPlates = Constructor(6)
	stackOfPlates.Pop() // -1
	stackOfPlates.Pop() // -1
	stackOfPlates.PopAt(1) // -1
	stackOfPlates.PopAt(3) // -1
	stackOfPlates.Pop() // -1
	stackOfPlates.Push(40) // null
	stackOfPlates.Push(10) // null
	stackOfPlates.Push(44) // null
	stackOfPlates.Push(44) // null
	stackOfPlates.Pop() // 44
	stackOfPlates.Push(24) // null
	stackOfPlates.Push(42) // null
	stackOfPlates.PopAt(4) // 4
	stackOfPlates.Pop() // 42
	stackOfPlates.PopAt(0) // 24
	//"pop", "pop", "popAt", "popAt", "pop", "push", "push", "push", "push", "pop", "push", "push", "popAt", "pop", "popAt"
	//[], [], [1], [3], [], [40], [10], [44], [44], [], [24], [42], [4], [], [0]
	//-1,-1,-1,-1,-1,null,null,null,null,44,null,null,-1,42,24
	//  "push", "push", "popAt", "pop", "popAt"
}

