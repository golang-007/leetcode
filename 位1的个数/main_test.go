package main

import "testing"


type testRet struct {
	OriginV uint32
	ExpectV int
}

func TestHammingWeight(t *testing.T)  {
	testMap := [...]testRet{
		{
			OriginV: 0b000010100101000001111010011100,
			ExpectV: 12,
		},
		{
			0b11111111111111111111111111111101,
			31,
		},
		{
			0b1,
			1,
		},
	}

	for _, v := range testMap {
		b := HammingWeight(v.OriginV)
		if v.ExpectV != b {
			t.Errorf("input %b, get %d, expect %d", v.OriginV, b, v.ExpectV)
		}
	}
}
