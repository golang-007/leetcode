package main

import (
	"fmt"
	"math"
)

func main()  {
	s := "    9223372036854775808"
	ss := MyAtoi(s)
	fmt.Println("ss 结果 ", ss)

}

func MyAtoi(s string) int {
	sign := 1
	//s = strings.Trim(s, " ") // 去除空格
	// 去除空格
	firstSpace := 0
	for index, v := range s {
		if v != ' ' { // 找到第一个不为空的 index
			firstSpace = index
			break
		}
	}
	s = s[firstSpace:]
	if len(s) == 0 {
		return 0
	}

	if s[0] == '-' || s[0] == '+' {
		if s[0] == '-' {
			sign = -1
		}
		s = s[1:]
	}

	// 记录第几次出现了非数字
	sIndex := 0
	ret := 0 // 结果
	for _,v := range []byte(s) {
		v -= '0' // 将 byte 类型转换为  unicode, 并比较运算
		if v > 9 {
			sIndex++
		}
		//fmt.Println(sIndex)
		if sIndex > 0{
			break
		}

		if ret * 10 + int(v) > math.MaxInt32 {
			if sign < 0 {
				return math.MinInt32
			} else {
				return math.MaxInt32
			}
		}

		ret = ret * 10 + int(v)
	}

	ret = ret * sign

	return ret
}