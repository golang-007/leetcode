package main

import (
	"testing"
)

type atioTest struct {
	Expect string
	Ret int
}

func TestMyAtoi(t *testing.T) {
	var testArr = [...]atioTest{
		{
			Expect: "42",
			Ret: 42,
		},
		{
			Expect: "   -42",
			Ret: -42,
		},
		{
			Expect: "4193 with words",
			Ret: 4193,
		},
		{
			Expect: "words and 987",
			Ret: 0,
		},
		{
			Expect: "-91283472332",
			Ret: -2147483648,
		},
		{
			Expect: "",
			Ret: 0,
		},
		{
			"9223372036854775808",
			2147483647,
		},
		{
			"",
			0,
		},
		{
			"2147483648",
			2147483647,
		},
		{
			"-2147483647",
			-2147483647,
		},
	}

	for _, v := range testArr {
		if vv := MyAtoi(v.Expect); v.Ret != vv {
			t.Errorf("input %s, get %d, but expect %d \n", v.Expect, vv, v.Ret)
		}
	}
}