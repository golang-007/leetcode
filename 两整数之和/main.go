package main

import "fmt"

//不使用运算符 + 和 - ，计算两整数 a 、b 之和。
//
// 示例 1:
//
// 输入: a = 1, b = 2
//输出: 3
//
//
// 示例 2:
//
// 输入: a = -2, b = 3
//输出: 1
// Related Topics 位运算
// 👍 346 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
func getSum(a int, b int) int {
	return a + b
}
//leetcode submit region end(Prohibit modification and deletion)
func main() {
	a := 1
	b := 5

	for b != 0 {
		c := (a & b) << 1 // 计算进位
		a = a ^ b         // 计算相同位两数的和
		b = c
	}

	fmt.Println(a)
}
